# DEP Practica

Deze repository bevat de practica voor de studenten van Declarative Programming. De huidige versie heeft betrekking op studiejaren 2021-2022 en 2022-2023. Er wordt momenteel aan gewerkt om het project te updaten voor het studiejaar 2023-2024. Het betreft kleine, quality-of-life verbeteringen en updates van de bijgeleverde instructies.

## Verplichte en optionele software

De volgende software is verplicht om met deze practica te kunnen starten:

- [Stack](https://docs.haskellstack.org/en/stable/) installeert ook de onderstaande punten wanneer je ze nodig hebt, op een geïsoleerde locatie:
  - [GHC](https://www.haskell.org/ghc/);
  - [Cabal](https://www.haskell.org/cabal/).

We adviseren jullie om Stack direct te installeren, in plaats van gebruik te maken van een van de bundels waar Stack ook in zit. Dit advies verschilt van voorgaande jaren. Meer informatie over (het gebruik van) Stack vind je in STACK.md in de root van deze Git.

De volgende software is optioneel, maar kan je helpen bij het ontwikkelen:

- We volgen de specificatie van [Haddock](https://haskell-haddock.readthedocs.io/en/latest/intro.html) voor de documentatie van jullie code. Je kunt de package zelf installeren als je je documentatie ook wilt omzetten naar een HTML-pagina, maar wij kijken alleen naar de comments in de code zelf;
- [HLint](https://hackage.haskell.org/package/hlint) geeft suggesties om je code te verbeteren;
- [GHCID](https://github.com/ndmitchell/ghcid) is een GHCI daemon, die je code compileert bij iedere save.

## Werken met/aan de practica

Elk van deze practica is een eigen Stack-project. Om het project te kunnen draaien moet je de voorgenoemde verplichte software installeren.
Vervolgens navigeer je in een elevated command prompt (command prompt, uitgevoerd als administrator) naar de hoofdmap van het practicum (bijv. pad/naar/hier/1-recursion-and-lists) en geef je het commando `stack run`.
Dit commando duurt de eerste keer ontzettend lang; laat maar gewoon draaien, dit blijft niet zo.

Door `stack run` aan te roepen voer je het hoofdprogramma uit, hetgeen ook (soms achter een keuzemenu) testcode bevat. **Bijna** alle opdrachten, behalve bijv. opdrachten waarbij je alleen documentatie moet schrijven, worden hierdoor getest tegen door ons geschreven cases. De feedback krijg je in leesbaar formaat in je console te zien. Wel zal die de eerste keer overwegend rood zijn; schrik daar niet van.

De code die je geacht wordt aan te passen vind je in het bestand src/Lib.hs van elk project. Het strekt ter aanbeveling om eerst het bestand en al het commentaar door te nemen, voordat je begint met aanpassen.
De aanpassingen die we van jullie vragen staan aangegeven met het woord TODO, in commentaar boven elke functie.

De vaakst voorkomende aanpassing is "Schrijf en documenteer deze functie." **Let op:** schrijf je commentaar in de Haddock-stijl! De twee vragen die goed commentaar sowieso beantwoordt zijn 1) wat doet deze functie? 2) hóé doet deze functie dat? Voor het gros van de functies is dit genoeg, maar voor de grotere functies is het handig om ook inline wat meer uitleg te geven over de werking.

Elk van de practica heeft tevens een eigen Readme, waar meer context in te vinden is. 

Heel veel succes met Haskell!