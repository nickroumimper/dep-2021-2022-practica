# Installatie en gebruik van de Haskell Tool Stack

Voor de practica van Declarative Programming leren we te programmeren in de taal Haskell. Om de code, inclusief de testsuites, te kunnen compileren en uitvoeren, maken we gebruik van de Haskell Tool Stack (vanaf dit punt: Stack). In dit document gaan we in meer detail in op hoe je Stack kunt installeren, wat je er allemaal mee kunt, welke errors onder ons bekend zijn en hoe je die kunt verhelpen.

## Installatiemethode

Er zijn meerdere pakketten beschikbaar die Stack op je systeem installeren. Elk van deze pakketten zou ervoor moeten zorgen dat je Stack-commando's kunt uitvoeren in je console. Om te checken dat dat lukt, kun je `stack --version` uitvoeren in je console:

```
> stack --version
Version 2.11.1, Git revision c1167a6abc3f4978ccded5ba0246a57387da0e2f x86_64 hpack-0.35.2
```

Uiteraard hoeft dit niet exact dezelfde versie te zijn als hierboven, dit is slechts een voorbeeld.

### Aanbevolen methode: Stack direct installeren

We adviseren jullie om Stack direct van de homepage van Stack zelf te halen ([link](https://docs.haskellstack.org/en/stable/)). Voor Linux-systemen en het overgrote merendeel van de Macs kun je het curl-commando uitvoeren dat wordt gegeven (aangenomen dat je curl hebt geïnstalleerd; zo niet, dan volstaat `sudo apt install curl` hoogstwaarschijnlijk). Voor Windows-systemen kun je de installer binnenhalen en uitvoeren; mocht dat niet lukken, dan kun je ook de executable zelf downloaden en handmatig aan je PATH toevoegen.

### Afgeraden methode: Installeren via GHCup

De primaire installer voor allerlei Haskellsoftware, genaamd GHCup ([link](https://www.haskell.org/ghcup/)), is ook in staat om Stack te installeren. Toch raden we dit af, om de volgende redenen:

- In het installatiescript van GHCup voor Windows moet je expliciet onthouden om Stack te installeren, want de default voor die keuze is om dat niet te doen;
- In onze ervaring moet het installatiescript van GHCup op Windows meermalen gedraaid worden voordat het ook echt werkt;
- GHCup haalt automatisch versies van de benodigde tools GHC en Cabal binnen, maar Stack haalt *ook zijn eigen versies van dezelfde tools binnen*. Zodoende heb je dus altijd meerdere kopieën van de tools op je systeem; als er errors optreden, kan het zijn dat je de verkeerde versie probeert te repareren.

Mocht je toch GHCup gebruiken, dan kan het prima zijn dat je het verschil nooit merkt; echter, als je tegen problemen aanloopt, houd dan in gedachten dat de tools die in GHCup staan *niet dezelfde tools zijn die Stack gebruikt*.

### Verouderde methode: Installeren via Haskell Platform

Voor 2022 maakte dit vak gebruik van Haskell Platform ([link](https://www.haskell.org/platform/)) om Stack te installeren. Echter, dit platform is inmiddels helaas verouderd en wordt niet meer onderhouden. Mocht je Stack indertijd geïnstalleerd hebben via Haskell Platform, bijvoorbeeld tijdens een eerdere iteratie van dit vak, dan kun je Stack gewoon blijven gebruiken, maar raden we je aan om deze even te updaten, met het commando `stack upgrade` in je console.

## Kleuren in de Windows-console

Stack zelf, en onze practicumprojecten, geven feedback via de console. Om die feedback leesbaarder te maken, wordt er gebruik gemaakt van speciale symbolen om consoletekst kleur te geven. Op Linux/Unix-systemen werken die kleuren altijd, maar op Windows-systemen (bij onze tests: in Windows 10) worden ze niet altijd weergegeven. In plaats daarvan zie je de symbolen zelf, wat ook de feedback van de testcode moeilijker leesbaar maakt. Voorbeeld:

```
←[31mOpgave 1 werkt nog niet, je error was:←[0m
←[1;30mPrelude.undefined
CallStack (from HasCallStack):
  error, called at libraries\base\GHC\Err.hs:75:14 in base:GHC.Err
  undefined, called at src\Lib.hs:25:7 in recursie-lijsten-0.1.0.0-L4MIFZBthqLGrQzNO4JkCK:Lib←[0m
←[0m←[0m
```

Windows-consoles zijn wel in staat om deze symbolen juist te interpreteren, maar deze optie staat standaard uit (heel handig). Om dit op te lossen, kun je een registry-commando uitvoeren in je console om deze optie aan te zetten. Het commando is als volgt (copy-paste en voer uit op de command line):

```
REG ADD HKCU\CONSOLE /f /v VirtualTerminalLevel /t REG_DWORD /d 1
```

Herstart hierna je console. Je hoeft deze aanpassing maar een keer te doen; ook na opnieuw opstarten blijft deze optie aan staan. Bij opnieuw uitvoeren zou je het bovenstaande voorbeeld zoals bedoeld moeten zien:

![Stack Run van blanco Practicum 1, met feedback in kleur](img/stack_colour_feedback.png)